#!/usr/bin/env python
import rospy
import time
import sys
import signal
import math
import numpy as np
from std_msgs.msg import Bool, Int32, Float32, Float32MultiArray

class scpgROS():
    def __init__(self):
	MotorTopic = "/morf_hw/multi_joint_command"
	jointTorqueTopic = "/morf_hw/joint_torques"
	rospy.init_node('scpg_pos')
	self.jointTorqueSub = rospy.Subscriber(jointTorqueTopic, Float32MultiArray, self.jointTorqueCallback)
	self.MotorPositionPub = rospy.Publisher(MotorTopic, Float32MultiArray, queue_size=1)
        self.log = open("morf_torque_output.csv", "w+")
	self.log.write("J-11\tJ-12\tJ-13\tJ-21\tJ-22\tJ-23\tJ-31\tJ-32\tJ-33\tJ-41\tJ-42\tJ-43\tJ-51\tJ-52\tJ-53\tJ-61\tJ-62\tJ-63\n")

    def jointTorqueCallback(self, data):
        self.jointTorques = data.data
	for item in self.jointTorques:
		item = item/149.795386991
		self.log.write("%f\t" % item)
	self.log.write("\n")

    def setLegMotorPosition(self, positions):
        self.MotorPositionPub.publish(Float32MultiArray(data=positions))

    def rescale(self, newMax, newMin, joint_value, joint_type):
	if joint_value  <= newMin and joint_type=='tc': joint_value = newMin
	elif joint_value >= newMax  and joint_type=='tc': joint_value = newMax
	if joint_value <= newMin  and joint_type=='cf': joint_value = newMin
	elif joint_value >= newMax  and joint_type=='cf': joint_value = newMax
        return joint_value


start_time_step=72500
joint_data_tc0=np.genfromtxt('spikedata_motor_tc_fl.csv',delimiter=',')
joint_data_cf0=np.genfromtxt('spikedata_motor_cf_fl.csv',delimiter=',')	
joint_data_tc1=np.genfromtxt('spikedata_motor_tc_ml.csv',delimiter=',')
joint_data_cf1=np.genfromtxt('spikedata_motor_cf_ml.csv',delimiter=',')
joint_data_tc2=np.genfromtxt('spikedata_motor_tc_bl.csv',delimiter=',')
joint_data_cf2=np.genfromtxt('spikedata_motor_cf_bl.csv',delimiter=',')
joint_data_tc3=np.genfromtxt('spikedata_motor_tc_fr.csv',delimiter=',')
joint_data_cf3=np.genfromtxt('spikedata_motor_cf_fr.csv',delimiter=',')
joint_data_tc4=np.genfromtxt('spikedata_motor_tc_mr.csv',delimiter=',')
joint_data_cf4=np.genfromtxt('spikedata_motor_cf_mr.csv',delimiter=',')
joint_data_tc5=np.genfromtxt('spikedata_motor_tc_br.csv',delimiter=',')
joint_data_cf5=np.genfromtxt('spikedata_motor_cf_br.csv',delimiter=',')
	

filter_size = 15	
count = start_time_step
max_cf_value = 2.2	
min_cf_value = 1.7
max_tc_value = 0.4	
min_tc_value = -0.4

scpgROS = scpgROS()
r = rospy.Rate(60)		#run at 60Hz

while count < (len(joint_data_tc1)-150000):        
    #find max value
    max_joint_value_tc0=np.max(joint_data_tc0[count-2000:count+2500])
    max_joint_value_cf0=np.max(joint_data_cf0[count-2500:count+2500])
    max_joint_value_tc1=np.max(joint_data_tc1[count-2000:count+2500])
    max_joint_value_cf1=np.max(joint_data_cf1[count-2500:count+2500])
    max_joint_value_tc2=np.max(joint_data_tc2[count-2000:count+2500])
    max_joint_value_cf2=np.max(joint_data_cf2[count-2500:count+2500])
    max_joint_value_tc3=np.max(joint_data_tc3[count-2000:count+2500])
    max_joint_value_cf3=np.max(joint_data_cf3[count-2500:count+2500])	    
    max_joint_value_tc4=np.max(joint_data_tc4[count-2500:count+2500])
    max_joint_value_cf4=np.max(joint_data_cf4[count-2500:count+2500])
    max_joint_value_tc5=np.max(joint_data_tc5[count-2500:count+2500])
    max_joint_value_cf5=np.max(joint_data_cf5[count-2500:count+2500])

    #convert spike count to radians
    joint_value_tc0=(np.average(joint_data_tc0[count:count+filter_size])/max_joint_value_tc0)*(math.pi/2)-(math.pi/4)
    joint_value_cf0=(np.average(joint_data_cf0[count:count+filter_size])/max_joint_value_cf0)*(math.pi/2)+(math.pi/2)
    joint_value_tc1=(np.average(joint_data_tc1[count:count+filter_size])/max_joint_value_tc1)*(math.pi/2)-(math.pi/4)
    joint_value_cf1=(np.average(joint_data_cf1[count:count+filter_size])/max_joint_value_cf1)*(math.pi/2)+(math.pi/2)
    joint_value_tc2=(np.average(joint_data_tc2[count:count+filter_size])/max_joint_value_tc2)*(math.pi/2)-(math.pi/4)
    joint_value_cf2=(np.average(joint_data_cf2[count:count+filter_size])/max_joint_value_cf2)*(math.pi/2)+(math.pi/2)
    joint_value_tc3=(np.average(joint_data_tc3[count:count+filter_size])/max_joint_value_tc3)*(-math.pi/2)+(math.pi/4)
    joint_value_cf3=(np.average(joint_data_cf3[count:count+filter_size])/max_joint_value_cf3)*(math.pi/2)+(math.pi/2)
    joint_value_tc4=(np.average(joint_data_tc4[count:count+filter_size])/max_joint_value_tc4)*(-math.pi/2)+(math.pi/4)
    joint_value_cf4=(np.average(joint_data_cf4[count:count+filter_size])/max_joint_value_cf4)*(math.pi/2)+(math.pi/2)
    joint_value_tc5=(np.average(joint_data_tc5[count:count+filter_size])/max_joint_value_tc5)*(-math.pi/2)+(math.pi/4)
    joint_value_cf5=(np.average(joint_data_cf5[count:count+filter_size])/max_joint_value_cf5)*(math.pi/2)+(math.pi/2)

    motor_positions = [11,scpgROS.rescale(max_tc_value, min_tc_value, joint_value_tc0,'tc'), #TC0
                   12,scpgROS.rescale(max_cf_value, min_cf_value, joint_value_cf0,'cf'), #CF0
                   13,-0.25, #CF0
                   21,scpgROS.rescale(max_tc_value, min_tc_value, joint_value_tc1,'tc'), #TC1
                   22,scpgROS.rescale(max_cf_value, min_cf_value, joint_value_cf1,'cf'), #CF1
		   23,-0.25, #CF1
		   31,scpgROS.rescale(max_tc_value, min_tc_value, joint_value_tc2,'tc'), #TC2
                   32,scpgROS.rescale(max_cf_value, min_cf_value, joint_value_cf2,'cf'), #CF2
		   33,-0.25, #CF2
		   41,scpgROS.rescale(max_tc_value, min_tc_value, joint_value_tc3,'tc'), #TC3
                   42,scpgROS.rescale(max_cf_value, min_cf_value, joint_value_cf3,'cf'), #CF3
		   43,-0.25, #CF3
		   51,scpgROS.rescale(max_tc_value, min_tc_value, joint_value_tc4,'tc'), #TC4
                   52,scpgROS.rescale(max_cf_value, min_cf_value, joint_value_cf4,'cf'), #CF4
		   53,-0.25, #CF4
		   61,scpgROS.rescale(max_tc_value, min_tc_value, joint_value_tc5,'tc'), #TC5
                   62,scpgROS.rescale(max_cf_value, min_cf_value, joint_value_cf5,'cf'), #CF5
		   63,-0.25] #CF5
   
    # Send rescaled CPG output to the motors
    scpgROS.setLegMotorPosition(motor_positions)
    #print motor_positions
    
    count += filter_size
    r.sleep()

#rospy.spin()


