#!/usr/bin/env python

#include <static_connection.h>
import nest
import nest.raster_plot
import numpy as np
import sys
import pylab
import math
import matplotlib.pyplot as plt
import random
import scipy
import scipy.fftpack

#Set parameters for network
time_resolution = 0.1
inh_neurons_count = 5
motor_neurons_count = 5
sim_time = 6000.0         #time in ms
test_runs = 50
V_th_min = -56.
V_th_max = -51.
freq_min = 2.
V_th = -50.

#neuron parameters
cpg1_neuronparams = {'C_m':200., 'g_L':10.,'E_L':-58.,'V_th':V_th,'Delta_T':2.,'tau_w':120., 'a':2., 'b':100., 'V_reset':-46., 'I_e':500.,'t_ref':2.0}    #adaptive exponential neuron params - (Naud,2008) paper, Figure 4D (regular bursting)
cpg2_neuronparams = {'C_m':200., 'g_L':10.,'E_L':-58.,'V_th':V_th,'Delta_T':2.,'tau_w':120., 'a':2., 'b':100., 'V_reset':-46., 'I_e':500.,'t_ref':2.0}    #adaptive exponential neuron params - (Naud,2008) paper, Figure 4D (regular bursting)
cpg3_neuronparams = {'C_m':200., 'g_L':10.,'E_L':-58.,'V_th':V_th,'Delta_T':2.,'tau_w':120., 'a':2., 'b':100., 'V_reset':-46., 'I_e':500.,'t_ref':2.0}    #adaptive exponential neuron params - (Naud,2008) paper, Figure 4D (regular bursting)
motor_neuronparams = {'C_m':200., 'g_L':10.,'E_L':-58.,'V_th':V_th,'Delta_T':2.,'tau_w':120., 'a':2., 'b':100., 'V_reset':-46., 'I_e':0.,'t_ref':2.0}    #adaptive exponential neuron params - (Naud,2008) paper, Figure 4D (regular bursting)

#spike detector parameters 
sd_params = {"withtime" : True, "withgid" : True, 'to_file' : False, 'flush_after_simulate' : False, 'flush_records' : True}

#connection parameters
cpg_syn_params = {"model":"static_synapse",
        "weight" : -10.,                        
        "delay" : 5.}
coupling_syn_params = {"model":"static_synapse_lbl",
        "synapse_label":1,
        "weight" : ((0.1*(abs(V_th_min)-abs(V_th)))-1.),   
        "delay" : 0.1}
static_coupling_syn_params = {"model":"static_synapse",
        "weight": -1.5,
        "delay" : 0.1}
interleg_coupling_syn_params = {"model":"static_synapse_lbl",
        "synapse_label":2,   
        "weight" : -10.,
        "delay" : 5.}
intersegment_coupling_syn_params = {"model":"static_synapse",   
        "weight" : -10.,
        "delay" : 5.}
phase0_syn_params = {"model":"static_synapse_lbl",
        "synapse_label":3,
        "weight" : 10.,
        "delay" : 0.1}
phase90_syn_params = {"model":"static_synapse_lbl",
        "synapse_label":4,
        "weight" : 10.,
        "delay" : 1000*(1/((freq_min+abs(V_th_min)-abs(V_th))*2))}     #V_th=-50 delay=166; V_th=-48 delay=100; V_th=-49 delay=125; V_th=-50.5 delay=200; V_th=-47 delay=83.3; V_th=-46 delay=71.4; V_th=-45 delay=62.5;
                
conn_dict_custom = {'rule': 'pairwise_bernoulli', 'p': 0.5}

#white noise parameters
noise_params = {"dt": 0.1, "std":100.}

class SpikingNeuralNetwork():
    def __init__(self):
        np.set_printoptions(precision=1,threshold=sys.maxsize)
        nest.set_verbosity('M_WARNING')
        nest.ResetKernel()
        nest.SetKernelStatus({"local_num_threads":1,"resolution":time_resolution})

	#Create noise
        self.white_noise = nest.Create("noise_generator",params=[noise_params])

        #Create neurons for cpgs - front left leg
        self.neuron_tc1_fl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        self.neuron_tc2_fl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        
        self.neuron_cf1_fl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        self.neuron_cf2_fl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        
        self.motor_neuron_tc_fl = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])      
        self.motor_neuron_cf_fl = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])

	#Create neurons for cpgs - front right leg
        self.neuron_tc1_fr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        self.neuron_tc2_fr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        
        self.neuron_cf1_fr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        self.neuron_cf2_fr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        
        self.motor_neuron_tc_fr = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])      
        self.motor_neuron_cf_fr = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])
        
        #Create neurons for cpgs - middle left leg
        self.neuron_tc1_ml = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        self.neuron_tc2_ml = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        
        self.neuron_cf1_ml = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        self.neuron_cf2_ml = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        
        self.motor_neuron_tc_ml = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])      
        self.motor_neuron_cf_ml = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])

	#Create neurons for cpgs - middle right leg
        self.neuron_tc1_mr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        self.neuron_tc2_mr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        
        self.neuron_cf1_mr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        self.neuron_cf2_mr = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        
        self.motor_neuron_tc_mr = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])      
        self.motor_neuron_cf_mr = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])

        #Create neurons for cpgs - back left leg
        self.neuron_tc1_bl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        self.neuron_tc2_bl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        
        self.neuron_cf1_bl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        self.neuron_cf2_bl = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        
        self.motor_neuron_tc_bl = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])      
        self.motor_neuron_cf_bl = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])

	#Create neurons for cpgs - back right leg
        self.neuron_tc1_br = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        self.neuron_tc2_br = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg1_neuronparams])
        
        self.neuron_cf1_br = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        self.neuron_cf2_br = nest.Create("aeif_cond_alpha",inh_neurons_count,params=[cpg2_neuronparams])
        
        self.motor_neuron_tc_br = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])      
        self.motor_neuron_cf_br = nest.Create("aeif_cond_alpha",motor_neurons_count,params=[motor_neuronparams])

        #Create spike detectors (connected to output neurons)
        self.spike_detector_motor_tc_fl = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        self.spike_detector_motor_cf_fl = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        
        self.spike_detector_motor_tc_ml = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        self.spike_detector_motor_cf_ml = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])

        self.spike_detector_motor_tc_bl = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        self.spike_detector_motor_cf_bl = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])

        self.spike_detector_motor_tc_fr = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        self.spike_detector_motor_cf_fr = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        
        self.spike_detector_motor_tc_mr = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        self.spike_detector_motor_cf_mr = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])

        self.spike_detector_motor_tc_br = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])
        self.spike_detector_motor_cf_br = nest.Create("spike_detector",motor_neurons_count,params=[sd_params])

        #Connect white noise to front leg neurons
        nest.Connect(self.white_noise,self.neuron_tc1_fl,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_tc2_fl,"all_to_all")
        
        nest.Connect(self.white_noise,self.neuron_cf1_fl,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_cf2_fl,"all_to_all")

        nest.Connect(self.white_noise,self.motor_neuron_tc_fl,"all_to_all")
        nest.Connect(self.white_noise,self.motor_neuron_cf_fl,"all_to_all")

	nest.Connect(self.white_noise,self.neuron_tc1_fr,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_tc2_fr,"all_to_all")
        
        nest.Connect(self.white_noise,self.neuron_cf1_fr,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_cf2_fr,"all_to_all")

        nest.Connect(self.white_noise,self.motor_neuron_tc_fr,"all_to_all")
        nest.Connect(self.white_noise,self.motor_neuron_cf_fr,"all_to_all")
        
        #Connect white noise to middle leg neurons
        nest.Connect(self.white_noise,self.neuron_tc1_ml,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_tc2_ml,"all_to_all")
        
        nest.Connect(self.white_noise,self.neuron_cf1_ml,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_cf2_ml,"all_to_all")

        nest.Connect(self.white_noise,self.motor_neuron_tc_ml,"all_to_all")
        nest.Connect(self.white_noise,self.motor_neuron_cf_ml,"all_to_all")

	nest.Connect(self.white_noise,self.neuron_tc1_mr,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_tc2_mr,"all_to_all")
        
        nest.Connect(self.white_noise,self.neuron_cf1_mr,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_cf2_mr,"all_to_all")

        nest.Connect(self.white_noise,self.motor_neuron_tc_mr,"all_to_all")
        nest.Connect(self.white_noise,self.motor_neuron_cf_mr,"all_to_all")

        #Connect white noise to back leg neurons
        nest.Connect(self.white_noise,self.neuron_tc1_bl,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_tc2_bl,"all_to_all")
        
        nest.Connect(self.white_noise,self.neuron_cf1_bl,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_cf2_bl,"all_to_all")

        nest.Connect(self.white_noise,self.motor_neuron_tc_bl,"all_to_all")
        nest.Connect(self.white_noise,self.motor_neuron_cf_bl,"all_to_all")

	nest.Connect(self.white_noise,self.neuron_tc1_br,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_tc2_br,"all_to_all")
        
        nest.Connect(self.white_noise,self.neuron_cf1_br,"all_to_all")
        nest.Connect(self.white_noise,self.neuron_cf2_br,"all_to_all")

        nest.Connect(self.white_noise,self.motor_neuron_tc_br,"all_to_all")
        nest.Connect(self.white_noise,self.motor_neuron_cf_br,"all_to_all")

        #Connect neurons to create cpgs - front leg
        nest.Connect(self.neuron_tc1_fl,self.neuron_tc2_fl,conn_dict_custom,cpg_syn_params)    
        nest.Connect(self.neuron_tc2_fl,self.neuron_tc1_fl,conn_dict_custom,cpg_syn_params)   
        
        nest.Connect(self.neuron_cf1_fl,self.neuron_cf2_fl,conn_dict_custom,cpg_syn_params)   
        nest.Connect(self.neuron_cf2_fl,self.neuron_cf1_fl,conn_dict_custom,cpg_syn_params)   

        nest.Connect(self.neuron_tc1_fr,self.neuron_tc2_fr,conn_dict_custom,cpg_syn_params)    
        nest.Connect(self.neuron_tc2_fr,self.neuron_tc1_fr,conn_dict_custom,cpg_syn_params)   
        
        nest.Connect(self.neuron_cf1_fr,self.neuron_cf2_fr,conn_dict_custom,cpg_syn_params)   
        nest.Connect(self.neuron_cf2_fr,self.neuron_cf1_fr,conn_dict_custom,cpg_syn_params) 
        
        #Connect neurons to create cpgs - middle leg
        nest.Connect(self.neuron_tc1_ml,self.neuron_tc2_ml,conn_dict_custom,cpg_syn_params)    
        nest.Connect(self.neuron_tc2_ml,self.neuron_tc1_ml,conn_dict_custom,cpg_syn_params)   
        
        nest.Connect(self.neuron_cf1_ml,self.neuron_cf2_ml,conn_dict_custom,cpg_syn_params)   
        nest.Connect(self.neuron_cf2_ml,self.neuron_cf1_ml,conn_dict_custom,cpg_syn_params)   

        nest.Connect(self.neuron_tc1_mr,self.neuron_tc2_mr,conn_dict_custom,cpg_syn_params)    
        nest.Connect(self.neuron_tc2_mr,self.neuron_tc1_mr,conn_dict_custom,cpg_syn_params)   
        
        nest.Connect(self.neuron_cf1_mr,self.neuron_cf2_mr,conn_dict_custom,cpg_syn_params)   
        nest.Connect(self.neuron_cf2_mr,self.neuron_cf1_mr,conn_dict_custom,cpg_syn_params) 

        #Connect neurons to create cpgs - back leg
        nest.Connect(self.neuron_tc1_bl,self.neuron_tc2_bl,conn_dict_custom,cpg_syn_params)    
        nest.Connect(self.neuron_tc2_bl,self.neuron_tc1_bl,conn_dict_custom,cpg_syn_params)   
        
        nest.Connect(self.neuron_cf1_bl,self.neuron_cf2_bl,conn_dict_custom,cpg_syn_params)   
        nest.Connect(self.neuron_cf2_bl,self.neuron_cf1_bl,conn_dict_custom,cpg_syn_params)   

        nest.Connect(self.neuron_tc1_br,self.neuron_tc2_br,conn_dict_custom,cpg_syn_params)    
        nest.Connect(self.neuron_tc2_br,self.neuron_tc1_br,conn_dict_custom,cpg_syn_params)   
        
        nest.Connect(self.neuron_cf1_br,self.neuron_cf2_br,conn_dict_custom,cpg_syn_params)   
        nest.Connect(self.neuron_cf2_br,self.neuron_cf1_br,conn_dict_custom,cpg_syn_params) 

        #Connect cpgs to motor neurons - front leg
        self.motor_connect_tc_fl = nest.Connect(self.neuron_tc1_fl,self.motor_neuron_tc_fl,conn_dict_custom,phase0_syn_params)
        self.motor_connect_cf_fl = nest.Connect(self.neuron_cf1_fl,self.motor_neuron_cf_fl,conn_dict_custom,phase90_syn_params)

        self.motor_connect_tc_fr = nest.Connect(self.neuron_tc1_fr,self.motor_neuron_tc_fr,conn_dict_custom,phase0_syn_params)
        self.motor_connect_cf_fr = nest.Connect(self.neuron_cf1_fr,self.motor_neuron_cf_fr,conn_dict_custom,phase90_syn_params)
        
        #Connect cpgs to motor neurons - middle leg
        self.motor_connect_tc_ml = nest.Connect(self.neuron_tc1_ml,self.motor_neuron_tc_ml,conn_dict_custom,phase0_syn_params)
        self.motor_connect_cf_ml = nest.Connect(self.neuron_cf1_ml,self.motor_neuron_cf_ml,conn_dict_custom,phase90_syn_params)

        self.motor_connect_tc_mr = nest.Connect(self.neuron_tc1_mr,self.motor_neuron_tc_mr,conn_dict_custom,phase0_syn_params)
        self.motor_connect_cf_mr = nest.Connect(self.neuron_cf1_mr,self.motor_neuron_cf_mr,conn_dict_custom,phase90_syn_params)

        #Connect cpgs to motor neurons - back leg
        self.motor_connect_tc_bl = nest.Connect(self.neuron_tc1_bl,self.motor_neuron_tc_bl,conn_dict_custom,phase0_syn_params)
        self.motor_connect_cf_bl = nest.Connect(self.neuron_cf1_bl,self.motor_neuron_cf_bl,conn_dict_custom,phase90_syn_params)

        self.motor_connect_tc_br = nest.Connect(self.neuron_tc1_br,self.motor_neuron_tc_br,conn_dict_custom,phase0_syn_params)
        self.motor_connect_cf_br = nest.Connect(self.neuron_cf1_br,self.motor_neuron_cf_br,conn_dict_custom,phase90_syn_params)
        
        #Connect neurons to couple cpgs (intra-leg)
        self.coupling_tccf1_fl = nest.Connect(self.neuron_tc1_fl,self.neuron_cf1_fl,"all_to_all",coupling_syn_params)  
        self.coupling_cftc1_fl = nest.Connect(self.neuron_cf1_fl,self.neuron_tc1_fl,"all_to_all",coupling_syn_params)   

        self.coupling_tccf2_fl = nest.Connect(self.neuron_tc2_fl,self.neuron_cf2_fl,"all_to_all",coupling_syn_params)  
        self.coupling_cftc2_fl = nest.Connect(self.neuron_cf2_fl,self.neuron_tc2_fl,"all_to_all",coupling_syn_params)   

        self.coupling_tccf1_fr = nest.Connect(self.neuron_tc1_fr,self.neuron_cf1_fr,"all_to_all",coupling_syn_params)  
        self.coupling_cftc1_fr = nest.Connect(self.neuron_cf1_fr,self.neuron_tc1_fr,"all_to_all",coupling_syn_params) 

        self.coupling_tccf2_fr = nest.Connect(self.neuron_tc2_fr,self.neuron_cf2_fr,"all_to_all",coupling_syn_params)  
        self.coupling_cftc2_fr = nest.Connect(self.neuron_cf2_fr,self.neuron_tc2_fr,"all_to_all",coupling_syn_params) 
        
        self.coupling_tccf1_ml = nest.Connect(self.neuron_tc1_ml,self.neuron_cf1_ml,"all_to_all",coupling_syn_params)  
        self.coupling_cftc1_ml = nest.Connect(self.neuron_cf1_ml,self.neuron_tc1_ml,"all_to_all",coupling_syn_params)   

        self.coupling_tccf2_ml = nest.Connect(self.neuron_tc2_ml,self.neuron_cf2_ml,"all_to_all",coupling_syn_params)  
        self.coupling_cftc2_ml = nest.Connect(self.neuron_cf2_ml,self.neuron_tc2_ml,"all_to_all",coupling_syn_params)  

        self.coupling_tccf1_mr = nest.Connect(self.neuron_tc1_mr,self.neuron_cf1_mr,"all_to_all",coupling_syn_params)  
        self.coupling_cftc1_mr = nest.Connect(self.neuron_cf1_mr,self.neuron_tc1_mr,"all_to_all",coupling_syn_params) 

        self.coupling_tccf2_mr = nest.Connect(self.neuron_tc2_mr,self.neuron_cf2_mr,"all_to_all",coupling_syn_params)  
        self.coupling_cftc2_mr = nest.Connect(self.neuron_cf2_mr,self.neuron_tc2_mr,"all_to_all",coupling_syn_params) 

        self.coupling_tccf1_bl = nest.Connect(self.neuron_tc1_bl,self.neuron_cf1_bl,"all_to_all",coupling_syn_params)  
        self.coupling_cftc1_bl = nest.Connect(self.neuron_cf1_bl,self.neuron_tc1_bl,"all_to_all",coupling_syn_params)

        self.coupling_tccf2_bl = nest.Connect(self.neuron_tc2_bl,self.neuron_cf2_bl,"all_to_all",coupling_syn_params)  
        self.coupling_cftc2_bl = nest.Connect(self.neuron_cf2_bl,self.neuron_tc2_bl,"all_to_all",coupling_syn_params)     

        self.coupling_tccf1_br = nest.Connect(self.neuron_tc1_br,self.neuron_cf1_br,"all_to_all",coupling_syn_params)  
        self.coupling_cftc1_br = nest.Connect(self.neuron_cf1_br,self.neuron_tc1_br,"all_to_all",coupling_syn_params) 

        self.coupling_tccf2_br = nest.Connect(self.neuron_tc2_br,self.neuron_cf2_br,"all_to_all",coupling_syn_params)  
        self.coupling_cftc2_br = nest.Connect(self.neuron_cf2_br,self.neuron_tc2_br,"all_to_all",coupling_syn_params) 

	#Connect neurons to couple cpgs (inter-leg)
	nest.Connect(self.neuron_tc1_fl,self.neuron_tc1_fr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_tc1_fr,self.neuron_tc1_fl,"all_to_all",interleg_coupling_syn_params)
	nest.Connect(self.neuron_tc2_fl,self.neuron_tc2_fr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_tc2_fr,self.neuron_tc2_fl,"all_to_all",interleg_coupling_syn_params)
	nest.Connect(self.neuron_cf1_fl,self.neuron_cf1_fr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_cf1_fr,self.neuron_cf1_fl,"all_to_all",interleg_coupling_syn_params)
	nest.Connect(self.neuron_cf2_fl,self.neuron_cf2_fr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_cf2_fr,self.neuron_cf2_fl,"all_to_all",interleg_coupling_syn_params)
	
        nest.Connect(self.neuron_tc1_ml,self.neuron_tc1_mr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_tc1_mr,self.neuron_tc1_ml,"all_to_all",interleg_coupling_syn_params)
        nest.Connect(self.neuron_tc2_ml,self.neuron_tc2_mr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_tc2_mr,self.neuron_tc2_ml,"all_to_all",interleg_coupling_syn_params)
	nest.Connect(self.neuron_cf1_ml,self.neuron_cf1_mr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_cf1_mr,self.neuron_cf1_ml,"all_to_all",interleg_coupling_syn_params)
	nest.Connect(self.neuron_cf2_ml,self.neuron_cf2_mr,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_cf2_mr,self.neuron_cf2_ml,"all_to_all",interleg_coupling_syn_params)
	
        nest.Connect(self.neuron_tc1_bl,self.neuron_tc1_br,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_tc1_br,self.neuron_tc1_bl,"all_to_all",interleg_coupling_syn_params)
        nest.Connect(self.neuron_tc2_bl,self.neuron_tc2_br,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_tc2_br,self.neuron_tc2_bl,"all_to_all",interleg_coupling_syn_params)
	nest.Connect(self.neuron_cf1_bl,self.neuron_cf1_br,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_cf1_br,self.neuron_cf1_bl,"all_to_all",interleg_coupling_syn_params)
	nest.Connect(self.neuron_cf2_bl,self.neuron_cf2_br,"all_to_all",interleg_coupling_syn_params)  
        nest.Connect(self.neuron_cf2_br,self.neuron_cf2_bl,"all_to_all",interleg_coupling_syn_params)

        #Connect neurons to couple cpgs (inter-segment)
        nest.Connect(self.neuron_tc1_fl,self.neuron_tc1_ml,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc1_ml,self.neuron_tc1_fl,"all_to_all",intersegment_coupling_syn_params)
        nest.Connect(self.neuron_tc2_fl,self.neuron_tc2_ml,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc2_ml,self.neuron_tc2_fl,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf1_fl,self.neuron_cf1_ml,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf1_ml,self.neuron_cf1_fl,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf2_fl,self.neuron_cf2_ml,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf2_ml,self.neuron_cf2_fl,"all_to_all",intersegment_coupling_syn_params)
	
        nest.Connect(self.neuron_tc1_mr,self.neuron_tc1_fr,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc1_fr,self.neuron_tc1_mr,"all_to_all",intersegment_coupling_syn_params)
        nest.Connect(self.neuron_tc2_mr,self.neuron_tc2_fr,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc2_fr,self.neuron_tc2_mr,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf1_mr,self.neuron_cf1_fr,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf1_fr,self.neuron_cf1_mr,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf2_mr,self.neuron_cf2_fr,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf2_fr,self.neuron_cf2_mr,"all_to_all",intersegment_coupling_syn_params)

        nest.Connect(self.neuron_tc1_ml,self.neuron_tc1_bl,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc1_bl,self.neuron_tc1_ml,"all_to_all",intersegment_coupling_syn_params)
        nest.Connect(self.neuron_tc2_ml,self.neuron_tc2_bl,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc2_bl,self.neuron_tc2_ml,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf1_ml,self.neuron_cf1_bl,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf1_bl,self.neuron_cf1_ml,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf2_ml,self.neuron_cf2_bl,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf2_bl,self.neuron_cf2_ml,"all_to_all",intersegment_coupling_syn_params)

        nest.Connect(self.neuron_tc1_mr,self.neuron_tc1_br,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc1_br,self.neuron_tc1_mr,"all_to_all",intersegment_coupling_syn_params)
        nest.Connect(self.neuron_tc2_mr,self.neuron_tc2_br,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_tc2_br,self.neuron_tc2_mr,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf1_mr,self.neuron_cf1_br,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf1_br,self.neuron_cf1_mr,"all_to_all",intersegment_coupling_syn_params)
	nest.Connect(self.neuron_cf2_mr,self.neuron_cf2_br,"all_to_all",intersegment_coupling_syn_params)  
        nest.Connect(self.neuron_cf2_br,self.neuron_cf2_mr,"all_to_all",intersegment_coupling_syn_params)

        #Connect spike detectors to neuron populations

        nest.Connect(self.motor_neuron_tc_fl,self.spike_detector_motor_tc_fl,"one_to_one")
        nest.Connect(self.motor_neuron_cf_fl,self.spike_detector_motor_cf_fl,"one_to_one")
        
        nest.Connect(self.motor_neuron_tc_ml,self.spike_detector_motor_tc_ml,"one_to_one")
        nest.Connect(self.motor_neuron_cf_ml,self.spike_detector_motor_cf_ml,"one_to_one")

        nest.Connect(self.motor_neuron_tc_bl,self.spike_detector_motor_tc_bl,"one_to_one")
        nest.Connect(self.motor_neuron_cf_bl,self.spike_detector_motor_cf_bl,"one_to_one")

        nest.Connect(self.motor_neuron_tc_fr,self.spike_detector_motor_tc_fr,"one_to_one")
        nest.Connect(self.motor_neuron_cf_fr,self.spike_detector_motor_cf_fr,"one_to_one")	
        
        nest.Connect(self.motor_neuron_tc_mr,self.spike_detector_motor_tc_mr,"one_to_one")
        nest.Connect(self.motor_neuron_cf_mr,self.spike_detector_motor_cf_mr,"one_to_one")	

        nest.Connect(self.motor_neuron_tc_br,self.spike_detector_motor_tc_br,"one_to_one")
        nest.Connect(self.motor_neuron_cf_br,self.spike_detector_motor_cf_br,"one_to_one")	

    def read_spikes(self,neuron_count,spike_detector):
	for i in range(neuron_count):
	    dSD = nest.GetStatus(spike_detector,keys="events")[i]
	    t_spikes = dSD['times']
	    spike_total_current = []
	    step = 0.
	    for n in range(int(sim_time/time_resolution)):
		spike_total_current.append(len(list(x for x in t_spikes if step <= x <= step+time_window)))
		step = step + time_resolution
	    if i == 0:
		spike_bins_current = spike_total_current
	    else:
		spike_bins_current = np.add(spike_bins_current,spike_total_current)
        return spike_bins_current

    def update_spike_threshold(self,neuron_population,offset):
        for neuron in neuron_population:
	    nest.SetStatus([neuron], {"V_th": V_th_min+offset})
	self.V_th = nest.GetStatus(neuron_population, keys="V_th")[0]
        return self.V_th
    
    def update_coupling_synapse_strength(self,synapse,label,spike_threshold):
	self.V_th = spike_threshold
        self.synapse = synapse
        self.label = label
	conns = nest.GetConnections(self.synapse, synapse_label=self.label)
	nest.SetStatus(conns,{"weight" : ((0.1*(abs(V_th_min)-abs(self.V_th)))-1.)})
        return conns

    def update_excitatory_synapse_strength(self,synapse,label,excitatory_weight):
        self.synapse = synapse
        self.label = label
        self.excitatory_weight = excitatory_weight
        conns = nest.GetConnections(self.synapse,synapse_label=self.label)
        nest.SetStatus(conns,{"weight" : self.excitatory_weight})
        return conns

    def update_synapse_delay(self,synapse,label,spike_threshold):
	self.V_th = spike_threshold
        self.synapse = synapse
        self.label = label
	conns = nest.GetConnections(self.synapse, synapse_label=self.label)
	nest.SetStatus(conns,{"delay" : 1000*(1/((freq_min+abs(V_th_min)-abs(self.V_th))*2))}) 
        return conns

#Run the network

snn = SpikingNeuralNetwork()
time_window = 50.
voltage_step = 1.
iterations = ((abs(V_th_min)-abs(V_th_max))+1)/voltage_step

#Set start time
time = nest.GetKernelStatus("time")

for i in range(int(iterations)):
    
    offset = 0.
    excitatory_weight = 250.
    #if i == 0:
    #    excitatory_weight = 2.
    #else:
    #    excitatory_weight = excitatory_weight*3
    #offset = i*voltage_step
    print('Offset, Excitatory Weight:')
    print offset, excitatory_weight

    #Update front leg neuron and network parameters
    snn.update_spike_threshold(snn.neuron_tc1_fl,offset)        
    snn.update_spike_threshold(snn.neuron_tc2_fl,offset)
    snn.update_spike_threshold(snn.neuron_cf1_fl,offset)
    snn.update_spike_threshold(snn.neuron_cf2_fl,offset)
    snn.update_spike_threshold(snn.motor_neuron_tc_fl,offset)
    threshold = snn.update_spike_threshold(snn.motor_neuron_cf_fl,offset)
    print('Voltage Threshold:')
    print threshold

    snn.update_spike_threshold(snn.neuron_tc1_fr,offset)        
    snn.update_spike_threshold(snn.neuron_tc2_fr,offset)
    snn.update_spike_threshold(snn.neuron_cf1_fr,offset)
    snn.update_spike_threshold(snn.neuron_cf2_fr,offset)
    snn.update_spike_threshold(snn.motor_neuron_tc_fr,offset)
    snn.update_spike_threshold(snn.motor_neuron_cf_fr,offset)
	
    new_weight = snn.update_coupling_synapse_strength(snn.coupling_tccf1_fl,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc1_fl,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_tccf2_fl,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc2_fl,1,threshold)
    snn.update_synapse_delay(snn.motor_connect_cf_fl,4,threshold)	#only update 90 degree phase connection
    snn.update_excitatory_synapse_strength(snn.motor_connect_tc_fl,3,excitatory_weight)
    snn.update_excitatory_synapse_strength(snn.motor_connect_cf_fl,4,excitatory_weight)
    print nest.GetStatus(new_weight,["weight"])[0]

    snn.update_coupling_synapse_strength(snn.coupling_tccf1_fr,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc1_fr,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_tccf2_fr,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc2_fr,1,threshold)
    snn.update_synapse_delay(snn.motor_connect_cf_fr,4,threshold)	#only update 90 degree phase connection
    snn.update_excitatory_synapse_strength(snn.motor_connect_tc_fr,3,excitatory_weight)
    snn.update_excitatory_synapse_strength(snn.motor_connect_cf_fr,4,excitatory_weight)
    
    #Update middle leg neuron and network parameters
    snn.update_spike_threshold(snn.neuron_tc1_ml,offset)        
    snn.update_spike_threshold(snn.neuron_tc2_ml,offset)
    snn.update_spike_threshold(snn.neuron_cf1_ml,offset)
    snn.update_spike_threshold(snn.neuron_cf2_ml,offset)
    snn.update_spike_threshold(snn.motor_neuron_tc_ml,offset)
    threshold = snn.update_spike_threshold(snn.motor_neuron_cf_ml,offset)
    
    snn.update_spike_threshold(snn.neuron_tc1_mr,offset)        
    snn.update_spike_threshold(snn.neuron_tc2_mr,offset)
    snn.update_spike_threshold(snn.neuron_cf1_mr,offset)
    snn.update_spike_threshold(snn.neuron_cf2_mr,offset)
    snn.update_spike_threshold(snn.motor_neuron_tc_mr,offset)
    snn.update_spike_threshold(snn.motor_neuron_cf_mr,offset)
	
    snn.update_coupling_synapse_strength(snn.coupling_tccf1_ml,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc1_ml,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_tccf2_ml,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc2_ml,1,threshold)
    snn.update_synapse_delay(snn.motor_connect_cf_ml,4,threshold)	#only update 90 degree phase connection
    snn.update_excitatory_synapse_strength(snn.motor_connect_tc_ml,3,excitatory_weight)
    snn.update_excitatory_synapse_strength(snn.motor_connect_cf_ml,4,excitatory_weight)

    snn.update_coupling_synapse_strength(snn.coupling_tccf1_mr,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc1_mr,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_tccf2_mr,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc2_mr,1,threshold)
    snn.update_synapse_delay(snn.motor_connect_cf_mr,4,threshold)	#only update 90 degree phase connection
    snn.update_excitatory_synapse_strength(snn.motor_connect_tc_mr,3,excitatory_weight)
    snn.update_excitatory_synapse_strength(snn.motor_connect_cf_mr,4,excitatory_weight)

    #Update back leg neuron and network parameters
    snn.update_spike_threshold(snn.neuron_tc1_bl,offset)        
    snn.update_spike_threshold(snn.neuron_tc2_bl,offset)
    snn.update_spike_threshold(snn.neuron_cf1_bl,offset)
    snn.update_spike_threshold(snn.neuron_cf2_bl,offset)
    snn.update_spike_threshold(snn.motor_neuron_tc_bl,offset)
    threshold = snn.update_spike_threshold(snn.motor_neuron_cf_bl,offset)
    
    snn.update_spike_threshold(snn.neuron_tc1_br,offset)        
    snn.update_spike_threshold(snn.neuron_tc2_br,offset)
    snn.update_spike_threshold(snn.neuron_cf1_br,offset)
    snn.update_spike_threshold(snn.neuron_cf2_br,offset)
    snn.update_spike_threshold(snn.motor_neuron_tc_br,offset)
    snn.update_spike_threshold(snn.motor_neuron_cf_br,offset)
	
    snn.update_coupling_synapse_strength(snn.coupling_tccf1_bl,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc1_bl,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_tccf2_bl,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc2_bl,1,threshold)
    snn.update_synapse_delay(snn.motor_connect_cf_bl,4,threshold)	#only update 90 degree phase connection
    snn.update_excitatory_synapse_strength(snn.motor_connect_tc_bl,3,excitatory_weight)
    snn.update_excitatory_synapse_strength(snn.motor_connect_cf_bl,4,excitatory_weight)

    snn.update_coupling_synapse_strength(snn.coupling_tccf1_br,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc1_br,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_tccf2_br,1,threshold)
    snn.update_coupling_synapse_strength(snn.coupling_cftc2_br,1,threshold)
    snn.update_synapse_delay(snn.motor_connect_cf_br,4,threshold)	#only update 90 degree phase connection
    snn.update_excitatory_synapse_strength(snn.motor_connect_tc_br,3,excitatory_weight)
    snn.update_excitatory_synapse_strength(snn.motor_connect_cf_br,4,excitatory_weight)

    nest.Simulate(sim_time/iterations)
	
#Create Rate Coded Output
spike_bins_motor_tc_fl = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_tc_fl)
spike_bins_motor_cf_fl = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_cf_fl)

spike_bins_motor_tc_fr = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_tc_fr)
spike_bins_motor_cf_fr = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_cf_fr)

spike_bins_motor_tc_ml = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_tc_ml)
spike_bins_motor_cf_ml = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_cf_ml)

spike_bins_motor_tc_mr = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_tc_mr)
spike_bins_motor_cf_mr = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_cf_mr)

spike_bins_motor_tc_bl = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_tc_bl)
spike_bins_motor_cf_bl = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_cf_bl)

spike_bins_motor_tc_br = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_tc_br)
spike_bins_motor_cf_br = snn.read_spikes(motor_neurons_count,snn.spike_detector_motor_cf_br)

#Save data to csv files
np.savetxt('spikedata_motor_tc_fl.csv',spike_bins_motor_tc_fl,delimiter=',')
np.savetxt('spikedata_motor_cf_fl.csv',spike_bins_motor_cf_fl,delimiter=',')

np.savetxt('spikedata_motor_tc_fr.csv',spike_bins_motor_tc_fr,delimiter=',')
np.savetxt('spikedata_motor_cf_fr.csv',spike_bins_motor_cf_fr,delimiter=',')

np.savetxt('spikedata_motor_tc_ml.csv',spike_bins_motor_tc_ml,delimiter=',')
np.savetxt('spikedata_motor_cf_ml.csv',spike_bins_motor_cf_ml,delimiter=',')

np.savetxt('spikedata_motor_tc_mr.csv',spike_bins_motor_tc_mr,delimiter=',')
np.savetxt('spikedata_motor_cf_mr.csv',spike_bins_motor_cf_mr,delimiter=',')

np.savetxt('spikedata_motor_tc_bl.csv',spike_bins_motor_tc_bl,delimiter=',')
np.savetxt('spikedata_motor_cf_bl.csv',spike_bins_motor_cf_bl,delimiter=',')

np.savetxt('spikedata_motor_tc_br.csv',spike_bins_motor_tc_br,delimiter=',')
np.savetxt('spikedata_motor_cf_br.csv',spike_bins_motor_cf_br,delimiter=',')

#Plot motor neuron outputs
pylab.figure(1)
pylab.subplot(211)
pylab.plot(spike_bins_motor_tc_fl,label='TC')
pylab.plot(spike_bins_motor_cf_fl,label='CF')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output front left leg joints')

pylab.subplot(212)
pylab.plot(spike_bins_motor_tc_fr,label='TC')
pylab.plot(spike_bins_motor_cf_fr,label='CF')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output front right leg joints')

pylab.figure(2)
pylab.subplot(211)
pylab.plot(spike_bins_motor_tc_ml,label='TC')
pylab.plot(spike_bins_motor_cf_ml,label='CF')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output middle left leg joints')

pylab.subplot(212)
pylab.plot(spike_bins_motor_tc_mr,label='TC')
pylab.plot(spike_bins_motor_cf_mr,label='CF')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output middle right leg joints')

pylab.figure(3)
pylab.subplot(211)
pylab.plot(spike_bins_motor_tc_bl,label='TC')
pylab.plot(spike_bins_motor_cf_bl,label='CF')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output back left leg joints')

pylab.subplot(212)
pylab.plot(spike_bins_motor_tc_br,label='TC')
pylab.plot(spike_bins_motor_cf_br,label='CF')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output back right leg joints')

pylab.figure(4)
pylab.subplot(211)
pylab.plot(spike_bins_motor_tc_fl,label='Front')
pylab.plot(spike_bins_motor_tc_ml,label='Middle')
pylab.plot(spike_bins_motor_tc_bl,label='Back')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output left legs')

pylab.subplot(212)
pylab.plot(spike_bins_motor_tc_fr,label='Front')
pylab.plot(spike_bins_motor_tc_mr,label='Middle')
pylab.plot(spike_bins_motor_tc_br,label='Back')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output right legs')

pylab.figure(5)
pylab.plot(spike_bins_motor_tc_fl,label='FL')
pylab.plot(spike_bins_motor_tc_ml,label='ML')
pylab.plot(spike_bins_motor_tc_bl,label='BL')
pylab.plot(spike_bins_motor_tc_fr,label='FR')
pylab.plot(spike_bins_motor_tc_mr,label='MR')
pylab.plot(spike_bins_motor_tc_br,label='BR')
pylab.xlabel('Time steps')
pylab.ylabel('Spikes')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
pylab.title('Rate-coded output all legs')

pylab.show()
